### Demo
* [news.bydovgal.com](https://news.bydovgal.com)

### Api documentation
* [Request docs](http://news.bydovgal.com:8000/api/request-docs)

### PHP extensions

* ```sudo apt-get install php-mbstring``` install php-mbstring extension
* ```sudo apt-get install php-curl``` install php-curl extension
* ```sudo apt-get install php-zip``` install php-zip extension
* ```sudo apt-get install php-dom``` install php-dom extension
* ```sudo apt-get install php-mysql``` install php-mysql extension


### Install

* ```cp .env.example .env``` make environment variables file from the .env.example 
* ```composer install ``` install dependencies
* ```php artisan:migrate ``` execute migrations
* ```php storage:link ``` create storage symlink

* ```* * * * * /usr/bin/php /var/www/news/backend/artisan schedule:run >> /dev/null 2>&1``` cron command for background schedules

### Load fixtures

* ```php artisan articles:receive``` load news from available services (TheGuardian, New York Times, News Api)
