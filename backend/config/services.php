<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'mail' => [
        'driver' => env('MAIL_DRIVER', 'none')
    ],

    'push' => [
        'driver' => env('PUSH_DRIVER', 'none')
    ],

    'news_api' => [
        'api_key' => env('NEWS_API_API_KEY', 'key')
    ],

    'the_guardian' => [
        'api_key' => env('THE_GUARDIAN_KEY', 'key')
    ],

    'new_york_times' => [
        'api_key' => env('NY_TIMES_API_KEY', 'key')
    ],

];
