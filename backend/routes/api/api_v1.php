<?php

use Illuminate\Support\Facades\Route;
use App\Http\Api\V1\Controllers\ArticleController;
use App\Http\Api\V1\Controllers\AuthController;
use App\Http\Api\V1\Controllers\FileController;
use App\Http\Api\V1\Controllers\LanguageController;
use App\Http\Api\V1\Controllers\ProfileController;
use App\Http\Api\V1\Controllers\PushTokenController;

Route::fallback(fn () => abort(404));

Route::prefix('auth')
    ->controller(AuthController::class)
    ->group(function ()
    {
        Route::post('send-code', 'sendCode')->middleware('throttle:send-code');
        Route::post('login', 'loginByIdentificator');
    });

Route::prefix('articles')
    ->controller(ArticleController::class)
    ->group(function ()
    {
        Route::get('', 'index');
        Route::get('{article}', 'show');
    });

Route::middleware('auth:sanctum')->group(function ()
{
    Route::delete('auth/logout', [AuthController::class, 'logout']);
    Route::post('push-token', [PushTokenController::class, 'store']);

    Route::prefix('storage')
        ->controller(FileController::class)
        ->group(function ()
        {
            Route::post('', 'store');
            Route::get('private/{filename}', 'showPrivate');
        });

    Route::prefix('profile')
        ->controller(ProfileController::class)
        ->group(function ()
        {
            Route::get('', 'show');
            Route::delete('', 'destroy');
            Route::patch('', 'update');
        });

    Route::prefix('articles')
        ->controller(ArticleController::class)
        ->group(function ()
        {
            Route::post('', 'store');
            Route::patch('{article}', 'update');
            Route::delete('{article}', 'destroy');
        });

    Route::prefix('languages')
        ->controller(LanguageController::class)
        ->group(function ()
        {
            Route::get('', 'index');
            Route::post('', 'store');
            Route::get('{language}', 'show');
            Route::patch('{language}', 'update');
            Route::delete('{language}', 'destroy');
        });
});
