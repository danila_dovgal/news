<?php

namespace App\Http\Services\Fractal;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use League\Fractal\Manager as Fractal;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class Manager
{
    public function __construct(
        private Fractal $fractal,
        private Container $container
    )
    {
        $fractal->setSerializer(new Serializer());
    }

    public function collection(
        iterable|LengthAwarePaginator $items,
        string|callable|object $transformer,
        ?string $type = null,
        bool $hasPagination = false
    ): array
    {
        $resource = new Collection($items, $this->transformer($transformer), $type);
        if ($hasPagination)
        {
            $resource->setPaginator(new IlluminatePaginatorAdapter($items));
        }

        if ($this->hasIncludes())
        {
            $this->fractal->parseIncludes($this->getIncludes());
        }

        return $this->fractal->createData($resource)->toArray();
    }

    private function transformer(string|callable|object $transformer): callable|object|string
    {
        if (!is_string($transformer))
        {
            return $transformer;
        }

        return $this->container->make($transformer);
    }

    private function hasIncludes(): bool
    {
        return !empty($_GET['include']);
    }

    private function getIncludes(): mixed
    {
        return $_GET['include'] ?? null;
    }

    public function item(
        array|object $item,
        string|callable|object $transformer,
        ?string $type = null
    ): array
    {
        $resource = new Item($item, $this->transformer($transformer), $type);

        if ($this->hasIncludes())
        {
            $this->fractal->parseIncludes($this->getIncludes());
        }

        return $this->fractal->createData($resource)->toArray();
    }
}
