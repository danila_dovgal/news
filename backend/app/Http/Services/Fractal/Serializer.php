<?php

namespace App\Http\Services\Fractal;

use League\Fractal\Serializer\ArraySerializer;

class Serializer extends ArraySerializer
{
    /**
     * @inheritdoc
     */
    public function collection($resourceKey, array $data): array
    {
        if (empty($resourceKey))
        {
            return $data;
        }

        return [$resourceKey => $data];
    }

    /**
     * @inheritdoc
     */
    public function item($resourceKey, array $data): array
    {
        if (empty($resourceKey))
        {
            return $data;
        }

        return [$resourceKey => $data];
    }
}
