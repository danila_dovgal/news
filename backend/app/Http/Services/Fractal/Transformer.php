<?php

namespace App\Http\Services\Fractal;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Primitive;
use League\Fractal\TransformerAbstract;

abstract class Transformer extends TransformerAbstract
{
    /**
     * @inheritdoc
     */
    protected function primitive($data, $transformer = null, $type = null): Primitive
    {
        return parent::primitive($data, $this->transformer($transformer), $type);
    }

    private function transformer(callable|object|string|null $transformer): callable|object|string|null
    {
        if (!is_string($transformer))
        { 
            return $transformer;
        }

        return app()->make($transformer);
    }

    /**
     * @inheritdoc
     */
    protected function item($data, $transformer, $type = null): Item
    {
        return parent::item($data, $this->transformer($transformer), $type);
    }

    /**
     * @inheritdoc
     */
    protected function collection($data, $transformer, $type = null): Collection
    {
        return parent::collection($data, $this->transformer($transformer), $type);
    }
}
