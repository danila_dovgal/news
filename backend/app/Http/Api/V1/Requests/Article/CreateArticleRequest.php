<?php

namespace App\Http\Api\V1\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class CreateArticleRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'author'        =>  'string|required',
            'title'         =>  'string|required',
            'description'   =>  'string|required',
            'content'       =>  'string|required',
            'origin'        =>  'string',
            'image_id'      =>  'exists:files,id',
        ];
    }
}
