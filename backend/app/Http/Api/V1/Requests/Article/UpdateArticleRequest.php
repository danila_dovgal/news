<?php

namespace App\Http\Api\V1\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticleRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'author'        =>  'string',
            'title'         =>  'string',
            'description'   =>  'string',
            'content'       =>  'string',
            'origin'        =>  'string',
            'image_id'      =>  'exists:files,id',
        ];
    }
}
