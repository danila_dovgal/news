<?php

namespace App\Http\Api\V1\Requests\PushToken;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $token
 */
class CreatePushTokenRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'token' => 'required|string',
        ];
    }
}
