<?php

namespace App\Http\Api\V1\Requests\File;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;

class StoreFileRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $maxFileSize = config('file.max_size');

        return [
            'file' => "required|file|max:$maxFileSize"
        ];
    }
}
