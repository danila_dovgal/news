<?php

namespace App\Http\Api\V1\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property bool $is_push_notifications_active
 */
class SubscriptionStatusRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'apple_iap_transaction_id' => 'string',
            'apple_iap_order_id' => 'string',
        ];
    }
}
