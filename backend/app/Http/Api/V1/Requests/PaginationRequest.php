<?php

namespace App\Http\Api\V1\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property ?int $per_page
 * @property ?int $current_page
 */
class PaginationRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'per_page'      =>  'int|min:1',
            'current_page'  =>  'int|min:1',
            'offset'        =>  'int',
            'limit'         =>  'int',
        ];
    }
}
