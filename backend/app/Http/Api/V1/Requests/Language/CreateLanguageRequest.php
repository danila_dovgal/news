<?php

namespace App\Http\Api\V1\Requests\Language;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $url
 */
class CreateLanguageRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'code'      =>  'string|required',
            'name'      =>  'string|required',
        ];
    }
}
