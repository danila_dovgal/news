<?php

namespace App\Http\Api\V1\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginByIdentificatorRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'identificator' => 'required|string',
            'code'  => 'required|string',
        ];
    }
}
