<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\V1\Requests\File\StoreFileRequest;
use App\Http\Api\V1\Transformers\FileTransformer;
use App\Services\File\Dto\StoreFileDto;
use App\Services\File\FileService;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Gate;

class FileController extends Controller
{
    public function __construct(private readonly FileService $fileService)
    {
    }

    public function store(StoreFileRequest $request): Response
    {
        Gate::authorize('create', File::class);

        $file = $request->file;

        $file = $this->fileService->store(
            new StoreFileDto(
                sourcePath: $file->path(),
                public: true,
            )
        );

        return $this->item(
            item: $file,
            transformer: FileTransformer::class
        );
    }

    public function showPublic($filename)
    {
        return Storage::disk('public')->get($filename);
    }

    public function showPrivate($filename)
    {
        Gate::authorize('show', File::class);

        return Storage::disk('private')->get($filename);
    }
}
