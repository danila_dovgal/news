<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\V1\Requests\Auth\SendCodeRequest;
use App\Http\Api\V1\Requests\Auth\LoginByIdentificatorRequest;
use App\Services\Auth\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    public function __construct(
        private readonly AuthService $authService
    )
    {
    }

    public function sendCode(SendCodeRequest $request): Response
    {
        $this->authService->sendCode($request->identificator);
        return response(null, 200);
    }

    public function loginByIdentificator(LoginByIdentificatorRequest $request): JsonResponse
    {
        if (!$token = $this->authService->loginByIdentificator($request->identificator, $request->code))
        {
            return response()->json(null, 422);
        }

        return response()->json(
            [
                'token' => $token,
            ]
        );
    }

    public function logout(): Response
    {
        $user = $this->getAuthUser();
        $this->authService->logout($user);
        return response(null, 200);
    }
}
