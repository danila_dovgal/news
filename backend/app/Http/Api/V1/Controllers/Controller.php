<?php

namespace App\Http\Api\V1\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Services\Fractal\Manager;
use App\Models\User;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    public function getAuthUser(): User
    {
        return auth()->user();
    }

    protected function item(
        object|array $item,
        callable|string|object $transformer,
        ?string $type = null
    ): Response
    {
        return $this->response($this->serializer()->item($item, $transformer, $type));
    }

    protected function collection(
        iterable $items,
        callable|string|object $transformer,
        ?string $type = null,
        bool $hasPagination = false
    ): Response
    {
        return $this->response($this->serializer()->collection($items, $transformer, $type, $hasPagination));
    }

    protected function serializer(): Manager
    {
        return app(Manager::class);
    }

    protected function response(array $data = null): Response
    {
        $response = app(ResponseFactory::class)->json($data);

        if ($data === null)
        {
            $response = $response->setContent(null);
        }

        return $response;
    }
}
