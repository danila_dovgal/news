<?php

namespace App\Http\Api\V1\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Services\User\UserProfileService;
use App\Http\Api\V1\Requests\User\UpdateUserProfileRequest;
use App\Http\Api\V1\Transformers\UserProfileTransformer;
use App\Services\User\Dto\UpdateUserProfileDto;

class ProfileController extends Controller
{
    public function __construct(
        private readonly UserProfileService $userProfileService,
    )
    {
    }

    public function show(): JsonResponse
    {
        $user = $this->getAuthUser();

        return $this->item(
            item: $user->load([]),
            transformer: UserProfileTransformer::class
        );
    }

    public function update(UpdateUserProfileRequest $request): Response
    {
        $user = $this->getAuthUser();

        $this->userProfileService->update(
            user: $user,
            dto: new UpdateUserProfileDto(
                name: $request->name,
                password: $request->password,
            )
        );

        return response(null, 200);
    }

    public function destroy(): Response
    {
        $user = $this->getAuthUser();
        $this->userProfileService->delete($user);

        return response(null, 200);
    }
}
