<?php

namespace App\Http\Api\V1\Controllers;

use App\Services\PushToken\PushTokenService;
use App\Services\PushToken\Dto\CreatePushTokenDto;
use App\Http\Api\V1\Requests\PushToken\CreatePushTokenRequest;
use App\Models\PushToken;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class PushTokenController extends Controller
{
    public function __construct(private readonly PushTokenService $pushTokenService)
    {
    }

    public function store(CreatePushTokenRequest $request): Response
    {
        Gate::authorize('create', PushToken::class);

        $user = $this->getAuthUser();

        $this->pushTokenService->create(new CreatePushTokenDto(
            userId: $user->id,
            token: $request->token,
            personalAccessTokenId: $user->currentAccessToken()->id,
        ));

        return response(null, 200);
    }
}
