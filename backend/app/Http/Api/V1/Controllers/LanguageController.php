<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\V1\Requests\PaginationRequest;
use App\Http\Api\V1\Requests\Language\CreateLanguageRequest;
use App\Http\Api\V1\Requests\Language\UpdateLanguageRequest;
use App\Http\Api\V1\Transformers\LanguageTransformer;
use App\Services\Language\Dto\CreateLanguageDto;
use App\Services\Language\Dto\UpdateLanguageDto;
use App\Services\Language\LanguageService;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\QueryBuilder;
use App\Models\Language;
use Illuminate\Support\Facades\Gate;

class LanguageController extends Controller
{
    public function __construct(
        private readonly LanguageService $languageService
    )
    {
    }

    public function index(PaginationRequest $request): JsonResponse
    {
        $with = [];

        $builder = QueryBuilder::for(Language::class)->with($with);

        $list = $builder->latest()->paginate(
            perPage: $request->per_page,
            page: $request->current_page
        );

        return $this->collection(
            items: $list,
            transformer: LanguageTransformer::class,
            type: 'data',
            hasPagination: $list instanceof LengthAwarePaginator
        );
    }

    public function show(Language $language): JsonResponse
    {
        return $this->item(
            item: $language,
            transformer: LanguageTransformer::class,
        );
    }

    public function store(CreateLanguageRequest $request): JsonResponse
    {
        Gate::authorize('create', Language::class);

        $language = $this->languageService->create(
            new CreateLanguageDto(
                code: $request->code,
                name: $request->name,
            )
        );

        return $this->item(
            item: $language->load([]),
            transformer: LanguageTransformer::class
        )->setStatusCode(Response::HTTP_CREATED);
    }

    public function update(UpdateLanguageRequest $request, Language $language): Response
    {
        Gate::authorize('update', $language);

        $this->languageService->update(
            language: $language,
            dto: new UpdateLanguageDto(
                name: $request->name,
            )
        );

        return response(null, 200);
    }

    public function destroy(Language $language): Response
    {
        Gate::authorize('delete', $language);

        $this->languageService->delete($language);
        return response(null, 200);
    }
}
