<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\V1\Requests\PaginationRequest;
use App\Http\Api\V1\Requests\Article\CreateArticleRequest;
use App\Http\Api\V1\Requests\Article\UpdateArticleRequest;
use App\Http\Api\V1\Transformers\ArticleTransformer;
use App\Services\Article\Dto\CreateArticleDto;
use App\Services\Article\Dto\UpdateArticleDto;
use App\Services\Article\ArticleService;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\QueryBuilder;
use App\Models\Article;
use Illuminate\Support\Facades\Gate;
use Spatie\QueryBuilder\AllowedFilter;

class ArticleController extends Controller
{
    public function __construct(
        private readonly ArticleService $articleService
    )
    {
    }

    public function index(PaginationRequest $request): JsonResponse
    {
        $with = ['file'];

        $builder = QueryBuilder::for(Article::class)
            ->with($with)
            ->defaultSort('-updated_at')
            ->allowedSorts(['created_at', 'updated_at'])
            ->allowedFilters([
                AllowedFilter::scope('text', 'text'),
                AllowedFilter::scope('author', 'author'),
                AllowedFilter::scope('start_date', 'start_date'),
                AllowedFilter::scope('end_date', 'end_date'),
            ]);;

        $list = $builder->latest()->paginate(
            perPage: $request->per_page,
            page: $request->current_page
        );

        return $this->collection(
            items: $list,
            transformer: ArticleTransformer::class,
            type: 'data',
            hasPagination: $list instanceof LengthAwarePaginator
        );
    }

    public function show(Article $article): JsonResponse
    {
        return $this->item(
            item: $article->load(['file']),
            transformer: ArticleTransformer::class,
        );
    }

    public function store(CreateArticleRequest $request): JsonResponse
    {
        Gate::authorize('create', Article::class);

        $user = $this->getAuthUser();

        $Article = $this->articleService->create(
            new CreateArticleDto(
                author: $request->author,
                title: $request->title,
                description: $request->description,
                content: $request->content,
                origin: $request->origin,
                imageId: $request->image_id,
            )
        );

        return $this->item(
            item: $Article->load(['file']),
            transformer: ArticleTransformer::class
        )->setStatusCode(Response::HTTP_CREATED);
    }

    public function update(UpdateArticleRequest $request, Article $article): Response
    {
        Gate::authorize('update', $article);

        $user = $this->getAuthUser();

        $this->articleService->update(
            article: $article,
            dto: new UpdateArticleDto(
                author: $request->author,
                title: $request->title,
                description: $request->description,
                content: $request->content,
                origin: $request->origin,
                imageId: $request->image_id,
            )
        );

        return response(null, 200);
    }

    public function destroy(Article $article): Response
    {
        Gate::authorize('delete', $article);

        $this->articleService->delete($article);
        return response(null, 200);
    }
}
