<?php

namespace App\Http\Api\V1\Transformers;

use App\Http\Services\Fractal\Transformer;
use App\Models\File;

class FileTransformer extends Transformer
{
    public function transform(File $file): array
    {
        return [
            'id' => $file->id,
            'url' => $file->url,
            'mime_type'  => $file->mime_type,
        ];
    }
}
