<?php

namespace App\Http\Api\V1\Transformers;

use App\Http\Services\Fractal\Transformer;
use App\Models\Language;

class LanguageTransformer extends Transformer
{
    public function __construct()
    {
    }

    public function transform(Language $language): array
    {
        return [
            'id'        =>   $language->id,
            'code'      =>   $language->code,
            'name'      =>   $language->name,
        ];
    }
}
