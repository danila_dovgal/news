<?php

namespace App\Http\Api\V1\Transformers;

use App\Http\Services\Fractal\Transformer;
use App\Models\User;

class UserProfileTransformer extends Transformer
{
    public function __construct()
    {
    }

    public function transform(User $user): array
    {
        return [
            'id'        =>   $user->id,
            'name'      =>   $user->name,
        ];
    }
}
