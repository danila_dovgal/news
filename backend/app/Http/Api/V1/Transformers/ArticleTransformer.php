<?php

namespace App\Http\Api\V1\Transformers;

use App\Http\Services\Fractal\Transformer;
use League\Fractal\Resource\Item;
use App\Models\Article;
use League\Fractal\Resource\Collection;

class ArticleTransformer extends Transformer
{
    protected array $defaultIncludes = ['image'];

    public function __construct()
    {
    }

    public function transform(Article $article): array
    {
        return [
            'id'            =>   $article->id,
            'author'        =>   $article->author,
            'title'         =>   $article->title,
            'description'   =>   $article->description,
            'content'       =>   $article->content,
            'origin'        =>   $article->origin,
            'created_at'    =>   $article->created_at,
            'updated_at'    =>   $article->updated_at,
        ];
    }

    public function includeImage(Article $article): ?Item
    {
        if ($article->relationLoaded('file') && $article->file)
        {
            return $this->item($article->file, FileTransformer::class);
        }

        return null;
    }
}
