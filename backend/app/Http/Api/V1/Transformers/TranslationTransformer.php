<?php

namespace App\Http\Api\V1\Transformers;

use App\Http\Services\Fractal\Transformer;
use App\Models\Translation;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;

class TranslationTransformer extends Transformer
{
    protected array $defaultIncludes = ['language'];

    public function __construct()
    {
    }

    public function transform(Translation $translation): array
    {
        $manager = new Manager();
        $fields = [];

        foreach ($translation->translation as $key => $value)
        {
            if (is_object($value))
            {
                $resource = $this->item($value, FileTransformer::class);
                $fields[$key] = $manager->createData($resource)->toArray();
            }
            else
            {
                $fields[$key] = $value;
            }
        }

        return [
            'fields' => $fields,
        ];
    }

    public function includeLanguage(Translation $translation): ?Item
    {
        if ($translation->relationLoaded('language') && $translation->language)
        {
            return $this->item($translation->language, LanguageTransformer::class);
        }

        return null;
    }
}
