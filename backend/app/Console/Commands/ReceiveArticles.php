<?php

namespace App\Console\Commands;

use App\Services\Article\ArticleService;
use Illuminate\Console\Command;

class ReceiveArticles extends Command
{

    protected $signature = 'articles:receive';

    protected $description = 'Receive articles from remote services';

    public function __construct(private readonly ArticleService $articleService)
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->articleService->receiveArticles();
        $this->info('Articles received');
    }
}
