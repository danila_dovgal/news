<?php

namespace App\Notifications;

use App\Models\User;
use App\Services\Mail\MailServiceInterface;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Notifications\Notification;
use Psr\Log\LoggerInterface;
use Throwable;

class MailChannel
{
    public function __construct(
        private readonly MailServiceInterface $mailService,
        private readonly LoggerInterface $logger
    )
    {
    }

    public function send(AnonymousNotifiable|User $notifiable, Notification $notification): void
    {
        try
        {
            $this->mailService->send($notifiable, $notification);
        }
        catch (Throwable $e)
        {
            $this->logger->warning($e->getMessage(), [
                'notifiable' => $notifiable,
                'notifier' => $this->mailService::class
            ]);
        }
    }
}
