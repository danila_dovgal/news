<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;

class NullChannel
{
    public function send(mixed $notifiable, Notification $notification): void
    {
    }
}
