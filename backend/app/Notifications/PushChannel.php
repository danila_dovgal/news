<?php

namespace App\Notifications;

use App\Models\User;
use App\Services\Push\PushServiceInterface;
use Illuminate\Notifications\Notification;
use Psr\Log\LoggerInterface;
use Throwable;

class PushChannel
{
    public function __construct(
        private readonly PushServiceInterface $pushService,
        private readonly LoggerInterface $logger
    )
    {
    }

    public function send(User $notifiable, Notification $notification): void
    {
        try
        {
            $this->pushService->send($notifiable, $notification);
        }
        catch (Throwable $e)
        {
            $this->logger->warning($e->getMessage(), [
                'user_id' => $notifiable->id,
                'notifier' => $this->pushService::class
            ]);
        }
    }
}
