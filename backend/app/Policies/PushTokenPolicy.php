<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PushTokenPolicy
{
    use HandlesAuthorization;

    public function create(User $user): bool
    {
        return $user->isUser();
    }
}
