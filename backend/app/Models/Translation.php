<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Translation extends Model
{
    use HasFactory;

    protected $fillable = [
        'translation',
    ];

    protected $casts = [
        'translation' => 'json',
    ];

    protected $with = [
        'language'
    ];

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class);
    }

    public function getTranslationAttribute()
    {
        $model                  =   $this->translationable_type;
        $translation            =   json_decode($this->attributes['translation']);
        $translationRules       =   $model::translationRules();
        $preparedTranslation    =   [];

        foreach ($translationRules as $param => $rules)
        {
            $field = $rules['field'];

            if (!isset($translation->$field))
            {
                continue;
            }

            switch ($rules['type'])
            {
                default:
                case 'string':
                    $preparedTranslation[$param] = $translation->$field ?? null;
                    break;
                case 'belongsTo':
                    if (class_exists($rules['model']))
                    {
                        $model = $rules['model'];
                        $preparedTranslation[$param] = $model::find($translation->$field);
                    }
                    else
                    {
                        $preparedTranslation[$param] = null;
                    }
                    break;
            }
        }

        return $preparedTranslation;
    }
}
