<?php

namespace App\Models;

use App\Models\Traits\Fileable;
use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    use Fileable;

    protected $fillable = [
        'author',
        'title',
        'description',
        'content',
        'origin',
        'service',
    ];

    public function scopeText(Builder $query, $text): Builder
    {
        return $query->where(function ($q) use ($text)
        {
            $q->orWhere('title', 'like', "%$text%")
                ->orWhere('description', 'like', "%$text%")
                ->orWhere('content', 'like', "%$text%");
        });
    }

    public function scopeAuthor(Builder $query, $author): Builder
    {
        return $query->where('author', 'like', "%$author%");
    }

    public function scopeStartDate(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', Carbon::parse($date));
    }

    public function scopeEndDate(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', Carbon::parse($date));
    }
}
