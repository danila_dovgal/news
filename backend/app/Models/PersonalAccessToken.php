<?php

namespace App\Models;

use Laravel\Sanctum\PersonalAccessToken as SanctumPersonalAccessToken;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PersonalAccessToken extends SanctumPersonalAccessToken
{
    public function pushTokens(): HasMany
    {
        return $this->hasMany(PushToken::class);
    }
}
