<?php

namespace App\Models\Traits;

use App\Models\Translation;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait Translationable
{
    public function translations(): MorphMany
    {
        return $this->morphMany(Translation::class, 'translationable');
    }

    abstract static function translationRules(): array;
}
