<?php

namespace App\Models;

use App\Models\Traits\Translationable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    use Translationable;

    protected $fillable = [
        'name',
        'description',
    ];

    public static function translationRules(): array
    {
        return [
            'name' => [
                'type' => 'string',
                'field' => 'name',
            ],
            'description' => [
                'type' => 'string',
                'field' => 'description',
            ],
        ];
    }
}
