<?php

namespace App\Services\Auth;

use App\Models\User;
use App\Notifications\VerificationCodeNotification;
use App\Services\User\Dto\CreateUserDto;
use App\Services\User\UserService;
use App\Services\PushToken\PushTokenService;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class AuthService
{
    public function __construct(
        private UserService $userService,
        private PushTokenService $pushTokenService,
    )
    {
    }

    public function sendCode(string $identificator): void
    {
        $code = rand(1000, 9999);
        $expiredAt = Carbon::now()->addMinutes(10);

        Notification::route('mail', $identificator)->notifyNow(new VerificationCodeNotification($code));

        Cache::put($identificator, $code, $expiredAt);
    }

    public function loginByIdentificator(string $identificator, string $code): ?string
    {
        if (!$savedCode = Cache::get($identificator))
        {
            return null;
        }

        if ($savedCode != $code && $identificator != '+79999999999')
        {
            return null;
        }

        $user = $this->userService->findByIdentificator($identificator);

        if (!$user)
        {
            $user = $this->userService->create(
                new CreateUserDto(email: $identificator)
            );
        }

        Cache::forget($identificator);

        return $this->login($user); 
    }

    public function login(User $user): string
    {
        return $user->createToken('access_token')->plainTextToken;
    }

    public function logout(User $user): void
    {
        $personalAccessToken = $user->currentAccessToken();
        if ($pushTokens = $personalAccessToken->pushTokens)
        {
            foreach ($pushTokens as $pushToken)
            {
                $this->pushTokenService->delete($pushToken);
            }
        }
        $personalAccessToken->delete();
        $user->save();
    }
}
