<?php

namespace App\Services\Mail;

use App\Models\User;
use Illuminate\Notifications\Notification;

class NullMailService implements MailServiceInterface
{
    public function send(mixed $notifiable, Notification $notification): bool
    {
        return true;
    }
}
