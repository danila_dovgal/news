<?php

namespace App\Services\Mail;

use App\Models\User;
use Illuminate\Notifications\Notification;

interface MailServiceInterface
{
    public function send(User $notifiable, Notification $notification): bool;
}
