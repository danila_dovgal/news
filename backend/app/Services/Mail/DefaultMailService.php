<?php

namespace App\Services\Mail;

use App\Models\User;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;

class DefaultMailService implements MailServiceInterface
{
    public function send(AnonymousNotifiable|User $notifiable, Notification $notification): bool
    {
        if ($notifiable::class == AnonymousNotifiable::class)
        {
            $email = $notifiable->routes['mail'];
        }
        else
        {
            $email = $notifiable->email;
        }

        Mail::to($email)->send($notification->toMail());

        return true;
    }
}
