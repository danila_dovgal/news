<?php

namespace App\Services\User\Dto;

class SubscriptionStatusDto
{
    public function __construct(
        public readonly ?string $appleIapTransactionId = null,
        public readonly ?string $appleIapOrderId = null,
    )
    {
    }
}
