<?php

namespace App\Services\User\Dto;

class CreateUserSubscriptionDto
{
    public function __construct(
        public readonly int $userId,
        public readonly int $subscriptionId,
        public readonly int $orderId,
        public readonly ?string $expiredAt,
    )
    {
    }
}
