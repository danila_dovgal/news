<?php

namespace App\Services\User\Dto;

class UpdateUserProfileDto
{
    public function __construct(
        public readonly ?string $name = null,
        public readonly ?string $password = null,
    )
    {
    }
}
