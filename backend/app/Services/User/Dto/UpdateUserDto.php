<?php

namespace App\Services\User\Dto;

use App\Models\Enums\Role;
use App\Models\Enums\SubscriptionType;

class UpdateUserDto
{
    public function __construct(
        public readonly ?string $name = null,
        public readonly ?string $email = null,
        public readonly ?string $phone = null,
        public readonly ?string $password = null,
        public readonly ?string $appleId = null,
        public readonly ?string $googleId = null,
        public readonly ?Role $role = null,
        public readonly ?SubscriptionType $subscriptionType = null,
    )
    {
    }
}
