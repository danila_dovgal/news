<?php

namespace App\Services\User;

use App\Models\User;
use App\Services\User\Dto\UpdateUserDto;
use App\Services\User\Dto\UpdateUserProfileDto;
use App\Traits\ManageTransactions;

class UserProfileService
{
    use ManageTransactions;

    public function __construct(
        private readonly UserService $userService,
    )
    {
    }

    public function update(User $user, UpdateUserProfileDto $dto): void
    {
        $this->transaction(function () use ($user, $dto)
        {
            $this->userService->update(
                user: $user,
                dto: new UpdateUserDto(
                    name: $dto->name,
                    password: $dto->password,
                )
            );
        });
    }

    public function delete(User $user): void
    {
        $this->userService->delete($user);
    }
}
