<?php

namespace App\Services\User;

use App\Models\PushToken;
use App\Services\PushToken\PushTokenService;
use App\Services\User\Dto\CreateUserDto;
use App\Services\User\Dto\UpdateUserDto;
use App\Models\User;
use App\Traits\ManageTransactions;

class UserService
{
    use ManageTransactions;

    public function __construct(
        private readonly PushTokenService $pushTokenService
    )
    {
    }

    public function findByIdentificator(string $identificator): ?User
    {
        return User::where('email', $identificator)->orWhere('phone', $identificator)->first();
    }

    public function create(CreateUserDto $dto): User
    {
        $data = $this->mapDtoToArray($dto);
        return User::create(array_filter($data));
    }

    public function update(User $user, UpdateUserDto $dto): void
    {
        $data = $this->mapDtoToArray($dto);
        $user->update(array_filter($data));
    }

    public function delete(User $user): void
    {
        $this->transaction(function () use ($user)
        {
            $user->pushTokens->each(fn (PushToken $pushToken) => $this->pushTokenService->delete($pushToken));

            $user->delete();
        });
    }

    private function mapDtoToArray(CreateUserDto|UpdateUserDto $dto): array
    {
        return [
            'name' => $dto->name,
            'phone' => $dto->phone,
            'email' => $dto->email,
            'password' => $dto->password,
            'apple_id' => $dto->appleId,
            'google_id' => $dto->googleId,
            'role'  => $dto->role,
            'subscription_type' => $dto->subscriptionType,
        ];
    }
}
