<?php

namespace App\Services\Article\Dto;

class ReceiveArticleDto
{
    public function __construct(
        public readonly ?string $author = null,
        public readonly ?string $title = null,
        public readonly ?string $description = null,
        public readonly ?string $content = null,
        public readonly ?string $origin = null,
        public readonly ?string $imageUrl = null,
        public readonly ?string $service = null,
    )
    {
    }
}
