<?php

namespace App\Services\Article;

use App\Services\Article\Dto\CreateArticleDto;
use App\Services\Article\Dto\UpdateArticleDto;
use App\Models\Article;
use App\Services\Article\Receive\ArticleReceiveService;
use App\Services\File\FileService;

class ArticleService
{
    public function __construct(
        private readonly ArticleReceiveService $articleReceiveService,
        private readonly FileService $fileService,
    )
    {
    }

    public function create(CreateArticleDto $dto): Article
    {
        $data = $this->mapDtoToArray($dto);
        $article = Article::updateOrCreate(array_filter($data));

        if ($dto->imageId)
        {
            $this->fileService->assignOwner($dto->imageId, $article);
        }

        return $article;
    }

    public function update(Article $article, UpdateArticleDto $dto): void
    {
        $data = $this->mapDtoToArray($dto);
        $article->update(array_filter($data));

        if ($dto->imageId)
        {
            $this->fileService->assignOwner($dto->imageId, $article);
        }
    }

    public function delete(Article $article): void
    {
        $article->delete();
    }

    private function mapDtoToArray(CreateArticleDto|UpdateArticleDto $dto): array
    {
        return [
            'author'        =>  $dto->author,
            'title'         =>  $dto->title,
            'description'   =>  $dto->description,
            'content'       =>  $dto->content,
            'origin'        =>  $dto->origin,
        ];
    }

    public function receiveArticles(): void
    {
        $articles = $this->articleReceiveService->receiveArticles();

        foreach ($articles as $article)
        {
            $imageId = null;
            if ($article->imageUrl)
            {
                if ($file = $this->fileService->storeFromUrl($article->imageUrl))
                {
                    $imageId = $file->id;
                }
            }

            $this->create(new CreateArticleDto(
                author: $article->author,
                title: $article->title,
                description: $article->description,
                content: $article->content,
                origin: $article->origin,
                imageId: $imageId,
            ));
        }
    }
}
