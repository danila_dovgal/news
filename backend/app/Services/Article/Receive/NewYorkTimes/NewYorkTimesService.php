<?php

namespace App\Services\Article\Receive\NewYorkTimes;

use App\Models\Article;
use App\Services\Article\Dto\ReceiveArticleDto;
use App\Services\Article\Receive\ArticleReceiveInterface;
use Exception;
use Illuminate\Support\Facades\Http;

class NewYorkTimesService implements ArticleReceiveInterface
{
    public function __construct(private readonly string $apiKey)
    {
    }

    public function receiveArticles(): array
    {
        $articles = [];
        $url = 'https://api.nytimes.com/svc/topstories/v2/us.json';

        $parameters = [
            'api-key' => $this->apiKey,
        ];

        try
        {
            $response = Http::get($url, $parameters);
            $data = json_decode($response->body());

            if (isset($data->results))
            {
                foreach ($data->results as $article)
                {
                    $articles[] = new ReceiveArticleDto(
                        author: $article->author ?? null,
                        title: $article->title ?? null,
                        description: $article->abstract ?? null,
                        origin: $article->url ?? null,
                        imageUrl: $article->multimedia[0]->url ?? null,
                        service: $this::class,
                    );
                }
            }
        }
        catch (Exception $e)
        {
        }

        return $articles;
    }

    public function receiveArticle(Article $article): ?ReceiveArticleDto
    {
        return new ReceiveArticleDto();
    }
}
