<?php

namespace App\Services\Article\Receive\TheGuardian;

use App\Models\Article;
use App\Services\Article\Dto\ReceiveArticleDto;
use App\Services\Article\Receive\ArticleReceiveInterface;
use Exception;
use Illuminate\Support\Facades\Http;

class TheGuardianService implements ArticleReceiveInterface
{
    public function __construct(private readonly string $apiKey)
    {
    }

    public function receiveArticles(): array
    {
        $articles = [];
        $url = 'https://content.guardianapis.com/search';

        $parameters = [
            'from-date' => date('Y-m-d'),
            'api-key' => $this->apiKey,
        ];

        try
        {
            $response = Http::get($url, $parameters);
            $data = json_decode($response->body());

            if (isset($data->response->results))
            {
                foreach ($data->response->results as $article)
                {
                    $articles[] = new ReceiveArticleDto(
                        author: $article->tags[0]->webTitle ?? null,
                        title: $article->webTitle ?? null,
                        description: $article->fields->headline ?? null,
                        origin: $article->webUrl ?? null,
                        imageUrl: $article->fields->thumbnail ?? null,
                        service: $this::class,
                    );
                }
            }
        }
        catch (Exception $e)
        {
        }

        return $articles;
    }

    public function receiveArticle(Article $article): ?ReceiveArticleDto
    {
        return new ReceiveArticleDto();
    }
}
