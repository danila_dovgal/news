<?php

namespace App\Services\Article\Receive;

use App\Models\Article;
use App\Services\Article\Dto\ReceiveArticleDto;

interface ArticleReceiveInterface
{
    public function receiveArticles(): ?array;
    public function receiveArticle(Article $article): ?ReceiveArticleDto;
}
