<?php

namespace App\Services\Article\Receive\NewsApi;

use App\Models\Article;
use App\Services\Article\Dto\ReceiveArticleDto;
use App\Services\Article\Receive\ArticleReceiveInterface;
use Exception;
use Illuminate\Support\Facades\Http;

class NewsApiService implements ArticleReceiveInterface
{
    public function __construct(private readonly string $apiKey)
    {
    }

    public function receiveArticles(): array
    {
        $articles = [];
        $url = 'https://newsapi.org/v2/top-headlines';

        $parameters = [
            'country' => 'us',
            'apiKey' => $this->apiKey,
        ];


        try
        {
            $response = Http::get($url, $parameters);
            $data = json_decode($response->body());

            if (isset($data->articles))
            {
                foreach ($data->articles as $article)
                {
                    $articles[] = new ReceiveArticleDto(
                        author: $article->author ?? null,
                        title: $article->title ?? null,
                        description: $article->description ?? null,
                        content: $article->content ?? null,
                        origin: $article->url ?? null,
                        imageUrl: $article->urlToImage ?? null,
                        service: $this::class,
                    );
                }
            }
        }
        catch (Exception $e)
        {
        }

        return $articles;
    }

    public function receiveArticle(Article $article): ?ReceiveArticleDto
    {
        return new ReceiveArticleDto();
    }
}
