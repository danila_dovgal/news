<?php

namespace App\Services\Article\Receive;

use App\Models\Article;
use App\Services\Article\Dto\ReceiveArticleDto;
use App\Services\Article\Receive\NewsApi\NewsApiService;
use App\Services\Article\Receive\NewYorkTimes\NewYorkTimesService;
use App\Services\Article\Receive\TheGuardian\TheGuardianService;

class ArticleReceiveService implements ArticleReceiveInterface
{
    public function __construct(
        private readonly NewsApiService $newsApiService,
        private readonly TheGuardianService $theGuardianService,
        private readonly NewYorkTimesService $newYorkTimesService,
    )
    {
    }

    public function receiveArticles(): array
    {
        $articles = [];

        $articles = array_merge($articles, $this->theGuardianService->receiveArticles());
        $articles = array_merge($articles, $this->newYorkTimesService->receiveArticles());
        $articles = array_merge($articles, $this->newsApiService->receiveArticles());

        return $articles;
    }

    public function receiveArticle(Article $article): ?ReceiveArticleDto
    {
        return new ReceiveArticleDto();
    }
}
