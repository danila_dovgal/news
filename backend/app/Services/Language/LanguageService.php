<?php

namespace App\Services\Language;

use App\Services\Language\Dto\CreateLanguageDto;
use App\Services\Language\Dto\UpdateLanguageDto;
use App\Models\Language;

class LanguageService
{
    public function create(CreateLanguageDto $dto): Language
    {
        $data = $this->mapDtoToArray($dto);
        return Language::create(array_filter($data));
    }

    public function update(Language $language, UpdateLanguageDto $dto): void
    {
        $data = $this->mapDtoToArray($dto);
        $language->update(array_filter($data));
    }

    public function delete(Language $language): void
    {
        $language->delete();
    }

    private function mapDtoToArray(CreateLanguageDto|UpdateLanguageDto $dto): array
    {
        return [
            'code' => $dto->code,
            'name' => $dto->name,
        ];
    }
}
