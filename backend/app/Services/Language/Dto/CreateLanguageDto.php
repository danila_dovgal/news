<?php

namespace App\Services\Language\Dto;

class CreateLanguageDto
{
    public function __construct(
        public readonly string $code,
        public readonly string $name,
    )
    {
    }
}
