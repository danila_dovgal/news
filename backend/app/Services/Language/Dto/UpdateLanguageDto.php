<?php

namespace App\Services\Language\Dto;

class UpdateLanguageDto
{
    public function __construct(
        public readonly ?string $code = null,
        public readonly ?string $name = null,
    )
    {
    }
}
