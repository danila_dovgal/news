<?php

namespace App\Services\Push;

use App\Models\User;
use App\Models\Enums\PushType;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Messaging\AndroidConfig;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification as FirebaseNotification;
use Illuminate\Support\Facades\Log;
use LogicException;

class FirebasePushService implements PushServiceInterface
{
    public function __construct(
        private readonly Messaging $messaging
    )
    {
    }

    public function send(User $notifiable, Notification $notification): bool
    {
        $type           =   $notification->type;
        $notification   =   $notification->toPush();

        $cloudMessage = CloudMessage::new()->withNotification(
            FirebaseNotification::create(
                $notification['title'],
                $notification['body'] ?? null,
            )
        );

        $tokens = [];

        switch ($notifiable::class)
        {
            case User::class:
                $tokens = $notifiable->pushTokens->pluck('token')->toArray();
                break;
        }

        if (!$tokens)
        {
            return false;
        }

        if (!$report = $this->messaging->sendMulticast($cloudMessage, $tokens))
        {
            return false;
        }

        foreach ($report->failures()->getItems() as $failure)
        {
            Log::info('Firebase push send report', ['error' => $failure->error()->getMessage()]);
        }
        Log::info('Firebase invalid push tokens', ['invalid_tokens' => $report->invalidTokens()]);

        return true;
    }
}
