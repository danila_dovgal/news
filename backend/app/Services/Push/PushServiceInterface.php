<?php

namespace App\Services\Push;

use App\Models\User;
use Illuminate\Notifications\Notification;

interface PushServiceInterface
{
    public function send(User $notifiable, Notification $notification): bool;
}
