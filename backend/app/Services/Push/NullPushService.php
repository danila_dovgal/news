<?php

namespace App\Services\Push;

use App\Models\User;
use Illuminate\Notifications\Notification;

class NullPushService implements PushServiceInterface
{
    public function send(User $notifiable, Notification $notification): bool
    {
        return true;
    }
}