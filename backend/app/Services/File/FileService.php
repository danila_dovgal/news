<?php

namespace App\Services\File;

use App\Models\File;
use App\Services\File\Dto\StoreFileDto;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class FileService
{

    public function __construct()
    {
    }

    public function store(StoreFileDto $dto): File
    {

        $sourcePath         =   $dto->sourcePath;
        $public             =   $dto->public;
        $mimeType           =   mime_content_type($sourcePath);
        $size               =   filesize($sourcePath);
        $extension          =   explode('/', $mimeType)[1];

        $fileName = uniqid() . '.' . $extension;

        $disk = 'private';
        if ($public)
        {
            $disk = 'public';
        }

        $destinationPath = Storage::putFileAs($disk, $sourcePath, $fileName);

        $file = File::create(
            [
                'storage'   => 'local',
                'path'      => $destinationPath,
                'mime_type' => $mimeType,
                'size'      => $size,
                'public'    => $public,
                'origin'    => $dto->origin,
            ]
        );

        return $file;
    }

    public function storeFromUrl(mixed $url): ?File
    {
        if ($file = File::where('origin', $url)->first())
        {
            return $file;
        }

        if (!$path = $this->downloadFile($url))
        {
            return null;
        }

        return $this->store(
            new StoreFileDto(
                sourcePath: $path,
                public: true,
                origin: $url,
            )
        );
    }

    public function delete(File $file): void
    {
        if (Storage::exists($file->path))
        {
            Storage::delete($file->path);
        }

        $file->delete();
    }

    public function assignOwner(File|int $file, ?Model $owner = null): File
    {
        if (is_int($file))
        {
            $file = File::find($file);
        }

        $file->fileable_type = $owner ? $owner::class : null;
        $file->fileable_id = $owner?->id;
        $file->save();

        return $file;
    }

    private function downloadFile(string $url): ?string
    {
        try
        {
            $response = Http::get($url);

            $tmpFilePath = sys_get_temp_dir() . '/' . uniqid();
            $tmpFile = fopen($tmpFilePath, 'w');
            fwrite($tmpFile, $response->body());
            fclose($tmpFile);

            return $tmpFilePath;
        }
        catch (Exception $e)
        {
            return null;
        }
    }
}
