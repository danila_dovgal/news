<?php

namespace App\Services\File\Dto;

class StoreFileDto
{
    public function __construct(
        public readonly string $sourcePath,
        public readonly bool $public = true,
        public readonly ?string $origin = null,
    )
    {
    }
}
