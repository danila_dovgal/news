<?php

namespace App\Services\PushToken\Dto;

class CreatePushTokenDto
{
    public function __construct(
        public readonly int $userId,
        public readonly string $token,
        public readonly int $personalAccessTokenId = 0,
    ) {
    }
}
