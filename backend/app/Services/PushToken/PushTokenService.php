<?php

namespace App\Services\PushToken;

use App\Models\PushToken;
use App\Services\PushToken\Dto\CreatePushTokenDto;
use App\Traits\ManageTransactions;

class PushTokenService
{
    use ManageTransactions;

    public function __construct()
    {
    }

    public function create(CreatePushTokenDto $dto): PushToken
    {
        return $this->transaction(function () use ($dto) {
            $pushToken = PushToken::updateOrCreate([
                'user_id' => $dto->userId,
                'token' => $dto->token,
                'personal_access_token_id' => $dto->personalAccessTokenId
            ]);

            return $pushToken;
        });
    }

    public function delete(PushToken $pushToken): void
    {
        $pushToken->delete();
    }

}