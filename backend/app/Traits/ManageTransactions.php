<?php

namespace App\Traits;

use Closure;
use Illuminate\Support\Facades\DB;
use Throwable;

trait ManageTransactions
{
    public function transaction(Closure $callback)
    {
        DB::beginTransaction();

        try {
            $callbackResult = $callback($this);
        } catch (Throwable $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        return $callbackResult;
    }
}
