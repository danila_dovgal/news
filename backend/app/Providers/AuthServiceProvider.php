<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\Article;
use App\Models\File;
use App\Models\Language;
use App\Models\PushToken;
use App\Policies\ArticlePolicy;
use App\Policies\FilePolicy;
use App\Policies\LanguagePolicy;
use App\Policies\PushTokenPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Article::class => ArticlePolicy::class,
        File::class => FilePolicy::class,
        Language::class => LanguagePolicy::class,
        PushToken::class => PushTokenPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();
    }
}
