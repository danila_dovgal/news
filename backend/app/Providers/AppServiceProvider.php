<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Sanctum;
use App\Models\PersonalAccessToken;
use App\Services\Article\Receive\NewsApi\NewsApiService;
use App\Services\Article\Receive\NewYorkTimes\NewYorkTimesService;
use App\Services\Article\Receive\TheGuardian\TheGuardianService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(
            NewsApiService::class,
            fn () => new NewsApiService(env('NEWS_API_API_KEY'))
        );

        $this->app->singleton(
            TheGuardianService::class,
            fn () => new TheGuardianService(env('THE_GUARDIAN_API_KEY'))
        );

        $this->app->singleton(
            NewYorkTimesService::class,
            fn () => new NewYorkTimesService(env('NEW_YORK_TIMES_API_KEY'))
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Sanctum::usePersonalAccessTokenModel(PersonalAccessToken::class);
    }
}
