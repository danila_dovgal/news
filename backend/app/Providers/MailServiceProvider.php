<?php

namespace App\Providers;

use App\Services\Mail\DefaultMailService;
use App\Services\Mail\NullMailService;
use App\Services\Mail\MailServiceInterface;
use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class MailServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(MailServiceInterface::class, fn () => $this->prepareNotifier());
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function prepareNotifier(): MailServiceInterface
    {
        $type = config('services.mail.driver');

        switch ($type)
        {
            case 'none':
                return new NullMailService();
            case 'default':
                return new DefaultMailService();
            default:
                throw new InvalidArgumentException("Unknown mail notifier <$type>.");
        }
    }
}
