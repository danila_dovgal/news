<?php

namespace App\Providers;

use App\Services\Push\FirebasePushService;
use App\Services\Push\NullPushService;
use App\Services\Push\PushServiceInterface;
use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;
use Kreait\Firebase\Factory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class PushServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(PushServiceInterface::class, fn () => $this->prepareNotifier());
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function prepareNotifier(): PushServiceInterface
    {
        $type = config('services.push.driver');

        switch ($type)
        {
            case 'none':
                return new NullPushService();
            case 'firebase':
                $messaging = (new Factory())
                    ->withServiceAccount(base_path(config('firebase.projects.app.credentials.file')))
                    ->createMessaging();
                return new FirebasePushService($messaging);
            default:
                throw new InvalidArgumentException("Unknown push notifier <$type>.");
        }
    }
}
