<?php

namespace App\Providers;

use App\Notifications\PushChannel;
use App\Notifications\NullChannel;
use App\Notifications\MailChannel;
use Illuminate\Notifications\ChannelManager;
use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->bootSmsChannel();
        $this->bootNullChannel();
        $this->bootPushChannel();
        $this->manager()->deliverVia('sms');
    }

    private function bootPushChannel(): void
    {
        $this->manager()->extend('push', function ()
        {
            return app(PushChannel::class);
        });
    }

    private function bootSmsChannel()
    {
        $this->manager()->extend('mail', function ()
        {
            return app(MailChannel::class);
        });
    }

    private function bootNullChannel(): void
    {
        $this->manager()->extend('null', function ()
        {
            return new NullChannel();
        });
    }

    private function manager(): ChannelManager
    {
        return $this->app->make(ChannelManager::class);
    }
}
