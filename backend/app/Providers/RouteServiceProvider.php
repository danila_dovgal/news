<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    public const HOME = '/home';
    protected Router $router;

    public function __construct($app)
    {
        parent::__construct($app);
        $this->router = app(Router::class);
    }

    public function boot(): void
    {
        $this->configureRateLimiting();
        $this->mapApiRoutes();
    }

    protected function mapApiRoutes()
    {
        $this->routes(function ()
        {
            Route::prefix('api')
                ->middleware('api')
                ->group(fn () => $this->prepareApiVersionRoutes('v1'));
        });
    }

    protected function prepareApiVersionRoutes(string $version): void
    {
        $version = strtolower($version);
        $middlewareGroup = "api.$version";
        $routes = base_path("routes/api/api_$version.php");
        $attributes = ['prefix' => $version];

        if ($this->router->hasMiddlewareGroup($middlewareGroup))
        {
            $attributes['middleware'] = $middlewareGroup;
        }

        Route::group($attributes, $routes);
    }

    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function (Request $request)
        {
            return Limit::perMinute(120)->by($request->user()?->id ?: $request->ip());
        });

        RateLimiter::for('send-code', function (Request $request)
        {
            return Limit::perMinute(1)->by($request->phone);
        });
    }
}
