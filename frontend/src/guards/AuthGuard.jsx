import React from 'react'
import { Navigate } from 'react-router-dom'

export default function AuthGuard(props) {
    let isAuth = true;

    if (!isAuth) {
        return <Navigate to="/" />
    }

    return props.element
}