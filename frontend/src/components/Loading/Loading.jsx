import 'assets/css/loading.css'

export default function Loading() {
    return (
        <div className="loading">
            <div className="rotator">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    )
}