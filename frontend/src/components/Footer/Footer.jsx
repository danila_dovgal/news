import 'assets/css/footer.css'

export default function Footer(props) {
    return (
        <footer>
            <div className='container'>
                <div className='logo'>News</div>
                <div>Made by Danila Dovgal</div>
            </div>
        </footer>
    )
}