import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { showAuthPopup, authLogout } from 'store/App/AppThunks'
import { Link } from 'react-router-dom'
import 'assets/css/header.css';

const mapStateToProps = state => ({
  isAuth: state.app.isAuth
})

const mapDispatchToProps = {
  showAuthPopup,
  authLogout
}

const connector = connect(mapStateToProps, mapDispatchToProps)

const Header = (props) => {

  const { isAuth, showAuthPopup, authLogout } = props

  return (
    <header>
      <div className="container">
        <Link className="logo" to="/">News</Link>
        <ul>
          <li><Link className="a" to="/">Home</Link></li>
          <li><Link className="a" to="/articles">All news</Link></li>
        </ul>
        {isAuth
          ? < div className="btn" onClick={authLogout}>Logout</div>
          : < div className="btn" onClick={showAuthPopup}>Login</div>
        }
      </div>
    </header >
  )
}

export default connector(Header)