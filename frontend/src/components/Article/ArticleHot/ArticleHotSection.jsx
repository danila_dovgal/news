import { useEffect } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { formatTime } from 'utils/TimeUtils'
import { getImage } from 'utils/FileUtils'
import { getArticleHotList } from 'store/Article/ArticleThunks'
import 'assets/css/articleHotSection.css'

const mapStateToProps = state => ({
    articleHotList: state.article.articleHotList
})

const mapDispatchToProps = {
    getArticleHotList
}

const connector = connect(mapStateToProps, mapDispatchToProps)

const ArticleHot = (props) => {

    const { articleHotList, getArticleHotList } = props

    useEffect(() => {
        if (!articleHotList.items.length) {
            getArticleHotList()
        }
    }, [getArticleHotList])

    let smallArticles = [];

    for (let i = 1; i <= 2; i++) {
        smallArticles.push(
            <Link target="_blank" to={articleHotList.items[i]?.origin} className="smallArticle">
                <div className="background" style={{ backgroundImage: 'url("' + getImage(articleHotList.items[i]?.image?.url) + '")' }}></div>
                <div className="time">{formatTime(articleHotList.items[i]?.created_at)}</div>
                <div className="title">{articleHotList.items[i]?.title}</div>
            </Link>
        )
    }

    return (
        <section className="articleHotSection">
            <div className="container reverse">
                <Link target="_blank" to={articleHotList.items[0]?.origin} className="bigArticle">
                    <div className="background" style={{ backgroundImage: 'url("' + getImage(articleHotList.items[0]?.image?.url) + '")' }}>
                        <div className="shadow"></div>
                    </div>
                    <h1 className="title">{articleHotList.items[0]?.title}</h1>
                    <div className="btn btn-light-tr">Continue reading</div>
                </Link>
                <div className="smallArticles">
                    <div className="title">Today</div>
                    {smallArticles}
                </div>
            </div>
        </section>
    )
}

export default connector(ArticleHot)