
import ArticleListItems from './ArticleListItems'
import 'assets/css/articleListSection.css'

export default function ArticleListSection(props) {
    return (
        <section className="articleListSection">
            {props.children}
            <ArticleListItems filters={props?.filters} />
        </section>
    )
}