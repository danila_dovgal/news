import { Link } from 'react-router-dom'
import { formatTime } from 'utils/TimeUtils'
import { getImage } from 'utils/FileUtils'
import 'assets/css/articleListItem.css'

export default function ArticleListItem(props) {
    return (
        <Link className="articleListItem" target="_blank" to={props.origin}>
            <div className="background" style={{ backgroundImage: 'url("' + getImage(props?.image?.url) + '")' }}></div>
            <div className="time">{formatTime(props.createdAt)}</div>
            <div className="title">{props.title}</div>
        </Link>
    )
}