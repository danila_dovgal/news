import { useEffect } from 'react'
import { connect } from 'react-redux'
import { getArticleList } from 'store/Article/ArticleThunks'
import ArticleListItem from './ArticleListItem'
import 'assets/css/articleListItems.css'

const mapStateToProps = state => ({
    articleList: state.article.articleList
})

const mapDispatchToProps = {
    getArticleList
}

const connector = connect(mapStateToProps, mapDispatchToProps)

const ArticleList = (props) => {
    const { articleList, getArticleList } = props

    useEffect(() => {
        if (!articleList.items.length) {
            getArticleList()
        }
    }, [])

    let articles = [];

    const pushArticle = (article) => {
        articles.push(<ArticleListItem
            key={article.id}
            title={article.title}
            description={article.description}
            content={article.content}
            origin={article.origin}
            createdAt={article.created_at}
            updatedAt={article.updated_at}
            image={article?.image}
        />
        )
    }

    if (props?.filters) {
        if (articleList.filters.text || articleList.filters.author || articleList.filters.startDate || articleList.filters.endDate) {
            articleList.items.forEach(article => {
                let text = articleList?.filters?.text?.toLowerCase()
                let author = articleList?.filters?.author?.toLowerCase()
                if (
                    (text &&
                        (article?.title?.toLowerCase().includes(text)
                            || article?.description?.toLowerCase().includes(text)
                        )) || (
                        articleList.filters.author && article?.author?.toLowerCase().includes(author)
                    )) {
                    pushArticle(article)
                }
            });
        } else {
            articleList.items.map((article) => pushArticle(article))
        }
    } else {
        articleList.items.map((article) => pushArticle(article))
    }

    return (
        <div className="container articleListItems">
            {articles}
        </div>
    )
}

export default connector(ArticleList)