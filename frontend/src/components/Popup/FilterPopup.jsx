import { useState } from 'react'
import { connect } from 'react-redux'
import { hidePopup } from 'store/App/AppActions'
import { articleApplyFilters, getArticleList } from 'store/Article/ArticleThunks'
import 'assets/css/filterPopup.css'

const mapStateToProps = state => ({
    filterPopup: state.app.filterPopup
})

const mapDispatchToProps = {
    articleApplyFilters,
    getArticleList,
    hidePopup,
}

const connector = connect(mapStateToProps, mapDispatchToProps)

const FilterPopup = (props) => {
    const { filterPopup, articleApplyFilters, getArticleList, hidePopup } = props
    const [query, setQuery] = useState('')
    const [author, setAuthor] = useState('')
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')

    const handleQuery = (event) => {
        setQuery(event.target.value)
    }

    const handleAuthor = (event) => {
        setAuthor(event.target.value)
    }

    const handleStartDate = (event) => {
        setStartDate(event.target.value)
    }

    const handleEndDate = (event) => {
        setEndDate(event.target.value)
    }

    const submit = (event) => {
        event.preventDefault()
        let filters = { text: query, author: author, startDate: startDate, endDate: endDate }
        articleApplyFilters(filters)
        getArticleList(filters)
        hidePopup()
    }

    return (
        <>
            {filterPopup
                ? <div className="filterPopup">
                    <div className="title">Filters</div>
                    <form onSubmit={submit}>
                        <label>Search query</label>
                        <input placeholder="Search query" type="text" onChange={handleQuery}></input>
                        <label>Author</label>
                        <input placeholder="Author" type="text" onChange={handleAuthor}></input>
                        <label>Start date</label>
                        <input placeholder="Start date" type="date" onChange={handleStartDate}></input>
                        <label>End date</label>
                        <input placeholder="End date" type="date" onChange={handleEndDate}></input>
                        <button className="btn" type="submit">Apply</button>
                    </form>
                </div >
                : (null)
            }
        </>
    )
}

export default connector(FilterPopup)