import { useState } from 'react'
import { connect } from 'react-redux'
import { authLogin, authCode, hidePopup } from 'store/App/AppThunks'
import 'assets/css/loginPopup.css'

const mapStateToProps = state => ({
    authPopup: state.app.authPopup,
    authCodeState: state.app.authCodeState,
    authLoginState: state.app.authLoginState,
})

const mapDispatchToProps = {
    hidePopup,
    authCode,
    authLogin,
}

const connector = connect(mapStateToProps, mapDispatchToProps)

const LoginPopup = (props) => {
    const { hidePopup, authCode, authLogin, authPopup, authCodeState, authLoginState } = props
    const [identificator, setIdentificator] = useState('')
    const [code, setCode] = useState('')

    const handleIdentificator = (event) => {
        setIdentificator(event.target.value)
    }

    const handleCode = (event) => {
        setCode(event.target.value)
    }

    const submit = (event) => {
        event.preventDefault()
        if (code && identificator && authLoginState !== 'requested') {
            authLogin({ identificator: identificator, code: code }).then(res => {
                hidePopup()
            })
        } else if (identificator && authCodeState !== 'requested') {
            authCode({ identificator: identificator })
        }
    }

    let codeField;

    if (authCodeState === 'success') {
        codeField = <input placeholder="Code" onChange={handleCode}></input>
    }

    let text;

    if (authCodeState === 'error') {
        text = 'Request code error'
    } else if (authCodeState === 'success') {
        text = 'A login code has been sent to your email'
    }

    return (
        <>
            {authPopup
                ? <div className="loginPopup">
                    <div className="title">Login</div>
                    <form onSubmit={submit}>
                        <input placeholder="Email" onChange={handleIdentificator}></input>
                        {codeField}
                        <div className="text">{text}</div>
                        <button className="btn" type="submit">Submit</button>
                    </form>
                </div >
                : (null)
            }
        </>
    )
}

export default connector(LoginPopup)