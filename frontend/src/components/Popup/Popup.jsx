import { connect } from 'react-redux'
import LoginPopup from './LoginPopup'
import FilterPopup from './FilterPopup'
import { hidePopup } from 'store/App/AppThunks'
import 'assets/css/popup.css'

const mapStateToProps = state => ({
    popup: state.app.popup
})

const mapDispatchToProps = {
    hidePopup
}

const connector = connect(mapStateToProps, mapDispatchToProps)

const Popup = (props) => {
    const { popup, hidePopup } = props

    return (
        <>
            {popup ?
                <div className="popup" >
                    <div className="background" onClick={() => hidePopup()}></div>
                    <LoginPopup />
                    <FilterPopup />
                </div>
                : (null)
            }
        </>
    )
}

export default connector(Popup)