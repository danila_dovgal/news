import { combineReducers } from 'redux'
import { AppReducers } from './App/AppReducers'
import { ArticleReducers } from './Article/ArticleReducers'

const reducers = combineReducers({
    app: AppReducers,
    article: ArticleReducers,
})

export default reducers