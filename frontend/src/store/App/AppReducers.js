import * as types from './AppConstants'
import produce from 'immer'

const initialState = {
  popup: false,
  authPopup: false,
  filterPopup: false,
  isAuth: localStorage.getItem('token') ? true : false,
  authCodeState: null,
  authLoginState: null,
}

export const AppReducers = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.HIDE_POPUP:
        draft.popup = false
        draft.authPopup = false
        draft.filterPopup = false
        break;
      case types.SHOW_AUTH_POPUP:
        draft.popup = true
        draft.authPopup = true
        break;
      case types.HIDE_AUTH_POPUP:
        draft.popup = false
        draft.authPopup = false
        break;
      case types.SHOW_FILTER_POPUP:
        draft.popup = true
        draft.filterPopup = true
        break;
      case types.HIDE_FILTER_POPUP:
        draft.popup = false
        draft.filterPopup = false
        break;
      case types.AUTH_CODE_REQUESTED:
        draft.authCodeState = 'requested'
        break;
      case types.AUTH_CODE_SUCCESS:
        draft.authCodeState = 'success'
        break;
      case types.AUTH_CODE_FAILED:
        draft.authCodeState = 'failed'
        break;
      case types.AUTH_LOGIN_REQUESTED:
        draft.authLoginState = 'requested'
        break;
      case types.AUTH_LOGIN_SUCCESS:
        localStorage.setItem('token', action.payload.token)
        draft.isAuth = true
        draft.authLoginState = 'success'
        break;
      case types.AUTH_LOGIN_FAILED:
        draft.authLoginState = 'failed'
        break;
      case types.AUTH_LOGOUT_SUCCESS:
        localStorage.removeItem('token')
        draft.isAuth = false
        break;
      default:
        return state
        break;
    }
  })