import * as actions from './AppActions'
import { ApiService } from 'services/ApiService'
import { ApiPaths } from 'constants/ApiPaths'

export const hidePopup = () => async dispatch => {
    return dispatch(actions.hidePopup())
}

export const showAuthPopup = () => async dispatch => {
    return dispatch(actions.showAuthPopup())
}

export const showFilterPopup = () => async dispatch => {
    return dispatch(actions.showFilterPopup())
}

export const authCode = (props) => dispatch => {
    dispatch(actions.authCodeRequested())
    console.log(props)
    return ApiService({ path: ApiPaths.AUTH_CODE, method: 'post', data: { identificator: props.identificator } })
        .then(res => dispatch(actions.authCodeSuccess(res)))
        .catch(err => dispatch(actions.authCodeFailed(err)))
}

export const authLogin = (props) => dispatch => {
    dispatch(actions.authLoginRequested())
    return ApiService({ path: ApiPaths.AUTH_LOGIN, method: 'post', data: { identificator: props.identificator, code: props.code } })
        .then(res => dispatch(actions.authLoginSuccess(res)))
        .catch(err => dispatch(actions.authLoginFailed(err)))
}

export const authLogout = (props) => dispatch => {
    return ApiService({ path: ApiPaths.AUTH_LOGOUT, method: 'delete' })
        .then(res => dispatch(actions.authLogoutSuccess(res)))
        .catch(err => dispatch(actions.authLogoutSuccess(err)))
}