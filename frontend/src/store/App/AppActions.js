import * as types from "./AppConstants"

export const showAuthPopup = () => ({
  type: types.SHOW_AUTH_POPUP
})

export const hideAuthPopup = () => ({
  type: types.HIDE_AUTH_POPUP
})

export const showFilterPopup = () => ({
  type: types.SHOW_FILTER_POPUP
})

export const hideFilterPopup = () => ({
  type: types.HIDE_FILTER_POPUP
})

export const hidePopup = () => ({
  type: types.HIDE_POPUP
})

export const authCodeRequested = () => ({
  type: types.AUTH_CODE_REQUESTED
})

export const authCodeSuccess = payload => ({
  type: types.AUTH_CODE_SUCCESS,
  payload
})

export const authCodeFailed = payload => ({
  type: types.AUTH_CODE_FAILED,
  payload
})

export const authLoginRequested = () => ({
  type: types.AUTH_LOGIN_REQUESTED
})

export const authLoginSuccess = payload => ({
  type: types.AUTH_LOGIN_SUCCESS,
  payload
})

export const authLoginFailed = payload => ({
  type: types.AUTH_LOGIN_FAILED,
  payload
})

export const authLogoutSuccess = payload => ({
  type: types.AUTH_LOGOUT_SUCCESS,
})