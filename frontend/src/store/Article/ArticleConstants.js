export const GET_ARTICLE_LIST_REQUESTED = 'GET_ARTICLE_LIST_REQUESTED'
export const GET_ARTICLE_LIST_SUCCESS = 'GET_ARTICLE_LIST_SUCCESS'
export const GET_ARTICLE_LIST_FAILED = 'GET_ARTICLE_LIST_FAILED'
export const GET_ARTICLE_HOT_LIST_REQUESTED = 'GET_ARTICLE_HOT_LIST_REQUESTED'
export const GET_ARTICLE_HOT_LIST_SUCCESS = 'GET_ARTICLE_HOT_LIST_SUCCESS'
export const GET_ARTICLE_HOT_LIST_FAILED = 'GET_ARTICLE_HOT_LIST_FAILED'
export const ARTICLE_APPLY_FILTERS = 'ARTICLE_APPLY_FILTERS'