import * as types from './ArticleConstants'
import produce from 'immer'
import { mergeByProperty } from 'utils/ArrayUtils'

const initialState = {
    articleHotList: {
        items: [],
        loading: false,
    },
    articleList: {
        items: [],
        loading: false,
        filters: {
            author: null,
            text: null,
            startDate: null,
            endDate: null,
        }
    },
}

export const ArticleReducers = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case types.GET_ARTICLE_LIST_REQUESTED:
                draft.articleList.loading = true
                break;
            case types.GET_ARTICLE_LIST_SUCCESS:
                draft.articleList.loading = false
                mergeByProperty(draft.articleList.items, action.payload.data, 'id')
                break;
            case types.GET_ARTICLE_LIST_FAILED:
                draft.articleList.loading = false
                break;
            case types.ARTICLE_APPLY_FILTERS:
                draft.articleList.filters = action.payload
                break;
            case types.GET_ARTICLE_HOT_LIST_REQUESTED:
                draft.articleHotList.loading = true
                break;
            case types.GET_ARTICLE_HOT_LIST_SUCCESS:
                draft.articleHotList.loading = false
                draft.articleHotList.items = action.payload.data
                break;
            case types.GET_ARTICLE_HOT_LIST_FAILED:
                draft.articleHotList.loading = false
                break;
            default:
                return state
        }
    })
