import * as actions from './ArticleActions'
import { ApiService } from 'services/ApiService'
import { ApiPaths } from 'constants/ApiPaths'

export const getArticleList = (props) => dispatch => {
    dispatch(actions.getArticleRequested())
    return ApiService({ path: ApiPaths.ARTICLE, method: 'get', filters: { text: props?.text, author: props?.author, start_date: props?.startDate, end_date: props?.endDate } })
        .then(res => dispatch(actions.getArticleListSuccess(res)))
        .catch(err => dispatch(actions.getArticleListFailed(err)))
}

export const getArticleHotList = () => dispatch => {
    dispatch(actions.getArticleHotRequested())
    return ApiService({ path: ApiPaths.ARTICLE, method: 'get' })
        .then(res => dispatch(actions.getArticleHotListSuccess(res)))
        .catch(err => dispatch(actions.getArticleHotListFailed(err)))
}

export const articleApplyFilters = (props) => dispatch => {
    dispatch(actions.articleApplyFilters(props))
}