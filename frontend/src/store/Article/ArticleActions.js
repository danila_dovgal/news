import * as types from "./ArticleConstants"

export const getArticleRequested = () => ({
  type: types.GET_ARTICLE_LIST_REQUESTED,
})

export const getArticleListSuccess = (payload) => ({
  type: types.GET_ARTICLE_LIST_SUCCESS,
  payload,
})

export const getArticleListFailed = () => ({
  type: types.GET_ARTICLE_LIST_FAILED,
})

export const getArticleHotRequested = () => ({
  type: types.GET_ARTICLE_HOT_LIST_REQUESTED,
})

export const getArticleHotListSuccess = (payload) => ({
  type: types.GET_ARTICLE_HOT_LIST_SUCCESS,
  payload,
})

export const getArticleHotListFailed = () => ({
  type: types.GET_ARTICLE_HOT_LIST_FAILED,
})

export const articleApplyFilters = (payload) => ({
  type: types.ARTICLE_APPLY_FILTERS,
  payload,
})