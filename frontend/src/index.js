import React from 'react';
import ReactDOM from "react-dom/client";
import { Provider } from 'react-redux';
import App from 'app/App';
import { store } from 'store/Store';

const app = ReactDOM.createRoot(document.getElementById('app'));

app.render(
    <Provider store={store}>
        <App />
    </Provider>,
);
