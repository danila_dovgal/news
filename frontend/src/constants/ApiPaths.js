export const ApiPaths = {
    ARTICLE: '/articles',
    AUTH_CODE: '/auth/send-code',
    AUTH_LOGIN: '/auth/login',
    AUTH_LOGOUT: '/auth/logout',
}