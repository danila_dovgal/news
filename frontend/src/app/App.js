import React from 'react';
import Router from 'routes/Router'
import 'assets/css/constants.css';
import 'assets/css/commons.css';

function App() {
    return <Router />
}

export default App;
