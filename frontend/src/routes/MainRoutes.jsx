import React, { lazy, Suspense } from 'react'
import { Route, Navigate } from 'react-router-dom'
import { AppPaths } from 'constants/AppPaths'
import Loading from 'components/Loading/Loading'
import UnAuthGuard from 'guards/UnAuthGuard'

const HomePage = lazy(() => import('pages/HomePage'))
const ArticleListPage = lazy(() => import('pages/Article/ArticleListPage'))

const MainRoutes = [
  <Route key='home' path='*' element={
    <Navigate to='/' />
  } />,
  <Route key='home' path='/' element={
    <UnAuthGuard element={
      <Suspense fallback={<Loading />}>
        <HomePage />
      </Suspense>
    } />
  } />,
  <Route key='articles' path='articles' element={
    <UnAuthGuard element={
      <Suspense fallback={<Loading />}>
        <ArticleListPage />
      </Suspense>
    } />
  } />
]

export default MainRoutes