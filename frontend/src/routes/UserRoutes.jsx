import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
import AuthGuard from 'guards/AuthGuard'
import Loading from 'components/Loading/Loading'

const UserRoutes = [
    <Route key='profile' path='/profile' element={
        <AuthGuard element={
            <Suspense fallback={<Loading />}>
                <div>awdawdaw</div>
            </Suspense>
        } />
    } />
]

export default UserRoutes