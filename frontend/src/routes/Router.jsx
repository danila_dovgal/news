import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainRoutes from './MainRoutes'
import UserRoutes from './UserRoutes'

export default function Router() {
  return (
    <BrowserRouter>
      <Routes>
        {MainRoutes}
        {UserRoutes}
      </Routes>
    </BrowserRouter>
  )
}