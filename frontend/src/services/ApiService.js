import axios from 'axios';

export const ApiService = (props) => {
  let path = process.env.REACT_APP_API_URL + props.path
  let data = {
    token: localStorage.getItem('token')
  }
  if (props.data) {
    Object.assign(data, props.data)
  }
  if (props.filters) {
    let filters = ''
    for (let key in props.filters) {
      if (props.filters[key]) {
        filters += `filter[${key}]=${props.filters[key]}`
        console.log(filters)
      }
    }
    if (filters !== '') {
      path += '?' + filters
    }
  }
  return new Promise((resolve, reject) => {
    return axios({
      method: props.method,
      url: path,
      data: data,
      withCredentials: false,
    }).then((data) => {
      resolve(data.data);
    }).catch((error) => {
      if (error?.response?.status === 401) {
        localStorage.removeItem('token')
        window.location.reload()
      }
      reject(error);
    })
  })
}
