import React from 'react'
import MainLayout from 'layouts/MainLayout'
import { Link } from 'react-router-dom'
import ArticleHotSection from 'components/Article/ArticleHot/ArticleHotSection'
import ArticleListSection from 'components/Article/ArticleList/ArticleListSection'

export default function HomePage() {
    return (
        <MainLayout>
            <ArticleHotSection></ArticleHotSection>
            <ArticleListSection>
                <div className="container caption">
                    <div className="block">
                        <h2 className="title">Hot news</h2>
                        <div className="subtitle">
                            <Link className="btn" to="/articles">All news</Link>
                            <Link className="btn" to="/articles">Hot news</Link>
                        </div>
                    </div>
                    <div className="block">
                        <Link className="btn" to="/articles">Show all news</Link>
                    </div>
                </div>
            </ArticleListSection>
        </MainLayout >
    )
}