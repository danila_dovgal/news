import React from 'react'
import { connect } from 'react-redux'
import MainLayout from 'layouts/MainLayout'
import { showFilterPopup } from 'store/App/AppThunks'
import ArticleListSection from 'components/Article/ArticleList/ArticleListSection'

const mapStateToProps = state => ({
    articleHotList: state.article.articleHotList
})

const mapDispatchToProps = {
    showFilterPopup
}

const connector = connect(mapStateToProps, mapDispatchToProps)

export const ArticleListPage = (props) => {
    const { showFilterPopup } = props

    return (
        <MainLayout>
            <ArticleListSection filters={true}>
                <div className="container caption">
                    <div className="block">
                        <h2 className="title">All news</h2>
                    </div>
                    <div className="block">
                        <div className="btn" onClick={showFilterPopup}>Filters</div>
                    </div>
                </div>
            </ArticleListSection>
        </MainLayout >
    )
}

export default connector(ArticleListPage)