import React from 'react'
import MainLayout from 'layouts/MainLayout'
import ArticleListSection from 'components/Article/ArticleList/ArticleListSection'

export default function ArticlePage() {
    return (
        <MainLayout>
            <ArticleListSection />
        </MainLayout >
    )
}