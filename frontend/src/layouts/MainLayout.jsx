import React from 'react'
import Header from 'components/Header/Header'
import Footer from 'components/Footer/Footer'
import Popup from 'components/Popup/Popup'
import 'assets/css/mainLayout.css'

import 'assets/css/popup.css'

export default function MainLayout(props) {
    const { children } = props
    return (
        <div className='mainLayout'>
            <Header />
            <div className="content">
                <main>
                    {children}
                </main>
            </div>
            <Popup />
            <Footer />
        </div>
    )
}