export const formatTime = (time) => {
    if (time) {
        return (new Date(time)).toLocaleString()
    }
    return null
}