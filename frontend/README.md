### Demo
* [news.bydovgal.com](https://news.bydovgal.com)

### Api documentation
* [Request docs](http://news.bydovgal.com:8000/api/request-docs)

### Getting started

install dependencies and start local dev server

```sh
npm install
npm start
```

### Details
- this example is using redux.js for application state management, to learn more about it I recommend [this tutorial](https://egghead.io/courses/getting-started-with-redux).
- you can look under the hood directly from your browser using [redux devtools](https://github.com/zalmoxisus/redux-devtools-extension). Install extension for your browser, open demo page (link above) and see how app state changes when you interact with it.
- if you wonder why reducers, actions and selectors are all in one file inside folder called `ducks`, [read more here](https://github.com/erikras/ducks-modular-redux).
